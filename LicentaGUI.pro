#-------------------------------------------------
#
# Project created by QtCreator 2016-05-15T20:42:51
#
#-------------------------------------------------

QT       += core gui
QMAKE_CXXFLAGS += -std=c++0x

CONFIG += debug_and_release 

CONFIG(debug, debug|release) {
    DESTDIR = build/debug
}
CONFIG(release, debug|release) {
    DESTDIR = build/release
}

OBJECTS_DIR = $$DESTDIR/.obj
MOC_DIR = $$DESTDIR/.moc
RCC_DIR = $$DESTDIR/.qrc
UI_DIR = $$DESTDIR/.u

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = LicentaGUI
TEMPLATE = app

INCLUDEPATH += ./sources
INCLUDEPATH += ./headers
INCLUDEPATH += ./forms
INCLUDEPATH += ./resources

SOURCES += sources/main.cpp\
           sources/mainwindow.cpp \
           sources/ABSpachet.cpp \
           sources/ARP.cpp \
           sources/ICMP.cpp \
           sources/UDP.cpp \
           sources/firexecutie.cpp

HEADERS  += headers/mainwindow.h \
            headers/pachete.hpp \
            headers/firexecutie.h

FORMS    += forms/mainwindow.ui

RESOURCES += resources/resource.qrc


