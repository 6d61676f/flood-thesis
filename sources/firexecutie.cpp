#include "firexecutie.h"

FirExecutie::FirExecutie(pachete::ABSpachet* pac, uint32_t milisecunde)
  : QThread()
{
  this->pachet = pac;
  this->milisecunde = milisecunde;
  this->iteratii = 1;
  if (milisecunde > 1000) {
    iteratii = 10;
  } else if (milisecunde >= 10000) {
    iteratii = 100;
  } else if (milisecunde >= 100000) {
    iteratii = 1000;
  }
  this->milisecunde /= iteratii;
}
void
FirExecutie::run()
{
  unsigned long sent;
  for (int j = 0; j < iteratii && this->stop == false; j++) {
    sent = this->pachet->Flood(this->milisecunde);
    emit semnalTrimis(sent);
    this->msleep(1500);
  }
}
void
FirExecutie::Stop()
{
  this->stop = true;
}
