#ifndef FIREXECUTIE_H
#define FIREXECUTIE_H
#include <QThread>
#include <pachete.hpp>
class FirExecutie : public QThread
{
  Q_OBJECT
protected:
  bool stop = false;
  pachete::ABSpachet* pachet;
  uint32_t milisecunde;
  void run();
  int iteratii;

signals:
  void semnalTrimis(unsigned long trimis);
private slots:
  void Stop();

public:
  FirExecutie(pachete::ABSpachet* pac, uint32_t milisecunde);
};

#endif // FIREXECUTIE_H
