#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <climits>

MainWindow::MainWindow(QWidget* parent)
  : QMainWindow(parent)
  , ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  this->setWindowTitle("Fl00d Meister");
  this->setWindowIcon(QIcon(":/icon/flood.svg"));

  if (getuid() != 0) {
    ui->textBrowser->setTextColor(QColor("red"));
    ui->textBrowser->append("FYI. Avem nevoie de root");
    ui->textBrowser->setTextColor(QColor("black"));
  }

  connect(ui->rbARP, SIGNAL(clicked(bool)), this, SLOT(enableInput(bool)));
  connect(ui->rbUDP, SIGNAL(clicked(bool)), this, SLOT(enableInput(bool)));
  connect(ui->rbICMP, SIGNAL(clicked(bool)), this, SLOT(enableInput(bool)));

  if (ui->rbARP->isChecked() == false && ui->rbICMP->isChecked() == false &&
      ui->rbUDP->isChecked() == false) {
    ui->lineSrc->setEnabled(false);
    ui->lineSrc->setPlaceholderText("Ip Sursa");
    ui->lineDst->setEnabled(false);
    ui->lineDst->setPlaceholderText("Ip Destinatie");
    ui->comboBox->setEnabled(false);
    ui->spinBoxMili->setMinimum(1);
    ui->spinBoxMili->setMaximum(600);
    ui->spinBoxMili->setEnabled(false);
    ui->buttonInit->setEnabled(false);
    ui->buttonFlood->setEnabled(false);
  }
}

MainWindow::~MainWindow()
{
  delete ui;

  this->quitFir(&this->f1);
  this->quitFir(&this->f2);
  this->delPack(&this->pack1);
  this->delPack(&this->pack2);
}

void
MainWindow::quitFir(FirExecutie** fir)
{
  if (*fir != nullptr) {
    while ((*fir)->isRunning()) {
      emit StopThread();
      (*fir)->wait(2500);
    }
    delete (*fir);
    (*fir) = nullptr;
  }
}

void
MainWindow::delPack(pachete::ABSpachet** pack)
{
  if ((*pack) != nullptr) {
    delete (*pack);
    (*pack) = nullptr;
  }
}
bool
MainWindow::getInterfete()
{
  struct ifaddrs *urmator, *curent;
  unsigned masca = IFF_BROADCAST | IFF_RUNNING | IFF_UP | IFF_MULTICAST;
  if (getifaddrs(&curent) == -1) {
    ui->textBrowser->append("Nu avem interfete! Eroare");
    return false;
  } else {
    for (urmator = curent; urmator != NULL; urmator = urmator->ifa_next) {
      if ((urmator->ifa_addr->sa_family == AF_INET) &&
          ((urmator->ifa_flags & masca) == masca)) {
        this->listaInterfete.append(urmator->ifa_name);
      }
    }
    freeifaddrs(curent);
    return true;
  }
}

void
MainWindow::initInterfete()
{
  ui->comboBox->setEnabled(true);
  if (this->listaInterfete.isEmpty()) {
    this->getInterfete();
    ui->comboBox->addItems(this->listaInterfete);
  }
}

void
MainWindow::enableInput(bool check)
{
  if (check) {
    ui->lineSrc->setEnabled(true);
    ui->lineDst->setEnabled(true);
    ui->spinBoxMili->setEnabled(true);
    ui->buttonInit->setEnabled(true);
    ui->comboBox->setEnabled(false);
    ui->buttonFlood->setEnabled(false);
  }
  if (ui->rbARP->isChecked()) {
    this->initInterfete();
  }
}

void
MainWindow::on_buttonInit_clicked()
{
  QString ipSrc = ui->lineSrc->text();
  QString ipDst = ui->lineDst->text();
  ui->buttonFlood->setEnabled(false);

  this->quitFir(&this->f1);
  this->quitFir(&this->f2);
  this->delPack(&this->pack1);
  this->delPack(&this->pack2);

  try {
    if (ui->rbUDP->isChecked()) {
      this->pack1 = new pachete::UDP(ipSrc.toStdString().c_str(),
                                     ipDst.toStdString().c_str());
    }
    if (ui->rbARP->isChecked()) {
      QString interfata = ui->comboBox->currentText();
      this->pack1 = new pachete::ARP(ipSrc.toStdString().c_str(),
                                     ipDst.toStdString().c_str(),
                                     interfata.toStdString().c_str());
      this->pack2 = new pachete::ARP(ipDst.toStdString().c_str(),
                                     ipSrc.toStdString().c_str(),
                                     interfata.toStdString().c_str());
    }
    if (ui->rbICMP->isChecked()) {
      this->pack1 = new pachete::ICMP(ipSrc.toStdString().c_str(),
                                      ipDst.toStdString().c_str());
    }
    ui->buttonFlood->setEnabled(true);
  } catch (const char* err) {
    ui->textBrowser->setTextColor(QColor("red"));
    ui->textBrowser->append(err);
    ui->textBrowser->setTextColor(QColor("black"));
  }
}

void
MainWindow::on_buttonFlood_clicked()
{
  if (ui->spinBoxMili->value() > ui->spinBoxMili->maximum()) {
    ui->spinBoxMili->setValue(ui->spinBoxMili->maximum());
  } else if (ui->spinBoxMili->value() < ui->spinBoxMili->minimum()) {
    ui->spinBoxMili->setValue(ui->spinBoxMili->minimum());
  }
  uint32_t milisecunde = ui->spinBoxMili->value() * 1000;

  this->quitFir(&this->f1);
  this->f1 = new FirExecutie(this->pack1, milisecunde);
  connect(this->f1, SIGNAL(semnalTrimis(unsigned long)), this,
          SLOT(pacheteTrimiseDeThread(unsigned long)));
  connect(this, SIGNAL(StopThread()), this->f1, SLOT(Stop()));
  this->f1->start();

  if (ui->rbARP->isChecked()) {
    this->quitFir(&this->f2);
    this->f2 = new FirExecutie(this->pack2, milisecunde);
    connect(this->f2, SIGNAL(semnalTrimis(unsigned long)), this,
            SLOT(pacheteTrimiseDeThread(unsigned long)));
    connect(this, SIGNAL(StopThread()), this->f2, SLOT(Stop()));
    this->f2->start();
  }
}

void
MainWindow::pacheteTrimiseDeThread(unsigned long val)
{
  QString mesaj = "Am trimis " + QString::number(val) + " de pachete.";
  ui->textBrowser->append(mesaj);
}
