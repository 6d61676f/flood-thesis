#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "firexecutie.h"
#include "pachete.hpp"
#include <QDebug>
#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget* parent = 0);
  ~MainWindow();

private:
  void quitFir(FirExecutie** fir);
  void delPack(pachete::ABSpachet** pack);
  QStringList listaInterfete;
  bool getInterfete(void);
  Ui::MainWindow* ui;
  void initInterfete(void);
  pachete::ABSpachet* pack1 = nullptr;
  pachete::ABSpachet* pack2 = nullptr;
  FirExecutie* f1 = nullptr;
  FirExecutie* f2 = nullptr;
private slots:
  void on_buttonInit_clicked();
  void enableInput(bool check);
  void on_buttonFlood_clicked();
  void pacheteTrimiseDeThread(unsigned long val);
signals:
  void StopThread(void);
};

#endif // MAINWINDOW_H
